@extends('master') 
@section('body') 
    <div class="container-fuild">
        <header class="nav header bg">
            <nav class="navbar navbar-light content">
                <img src="{{ asset('img/law-cut-icon_03.png') }}">
                <a class="navbar-brand" href="/">Home</a>
            </nav>
        </header>
        <div class="container-fuild content">
            <div class="row" style="margin-top:1%">
                <div class="col-md-9 col-xs-9 col-sm-9">
                    <div class="col-md-12">
                        <form action="{{route('search')}}" style="width:100%;" method="POST">
                                    @csrf
                                    <div class="input-group col-md-12 col-xs-12 col-sm-12">
                                        <input type="text" class="form-control search" name="txtSearch" id="autocomplete" required/>
                                        <button type="submit" class=" btn-success"><i class="fa fa-search"></i></button>
                                    </div>
                        </form>
                    </div>
                        <br>
                        <br>
                        <div class="col-md-12">
                            @if(!empty($chapter))
                            @foreach($chapter as $value)
                                <ul class="list-unstyled" id="iterm">
                                       
                                    <li class="media "><img class="mr-3 img-thumbnail" src="{{ asset('uploads/nothumbail.png') }}" alt="">
                                        <div class="media-body">
                                            <h5 class="mt-0 mb-1"><a href="/api/get-detail/{{ $value->section}}">{{$value->title}}</a></h5> 
                                            {!! str_limit($value->content,200) !!}
                                        </div>
                                    </li>
                                    
                                </ul>
                                @endforeach
                            @endif        
                        </div>
                   
                </div>
                <div class="col-md-3 col-xs-3 col-sm-3">
                    <img data-src="holder.js/200x200" src="{{asset('img/law-cut-icon_11.png')}}" data-holder-rendered="true" class="banner1">
                    <img data-src="holder.js/200x200" src="{{asset('img/law-cut-icon_39.png')}}" data-holder-rendered="true" class="banner2">
                </div>

            </div>
        </div>

        <footer>
            <div class="footer-copyright text-center py-3">© 2018 Copyright:
                <a href="#"> Hanbisoft</a>
            </div>
        </footer>
        <script>
                  var arr = [];
            $.ajax({
                url: "{{ route('api.search') }}",
                type: "get",
                dateType: "text",
                success: function(JsonResult) {
                for (var i  in JsonResult) {
                    arr.push(JsonResult[i])
                }
                console.log(arr)
                    $('#autocomplete').autocomplete({
                        lookup: arr,
                        onSelect: function(suggestion) {
                            console.log(suggestion)
                        }
                    });   
                }
            });
        </script>
    </div>
@endsection