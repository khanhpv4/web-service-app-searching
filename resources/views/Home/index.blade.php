
  @extends('master') 
  @section('body') 
    <div class="container-fuild">
        <header class="nav header bg">
            <nav class="navbar navbar-light content">
                <img src="{{ asset('img/law-cut-icon_03.png') }}">
                <a class="navbar-brand" href="/">Home</a>
                
            </nav>
        </header>
        <div class="container-fuild content">
            <div class="row" style="margin-top:1%">
                <div class="col-md-9 col-xs-9 col-sm-9">
                       <div class="row">
                        <div class="col-md-12" style="padding-right:3.1%;">
                            <form action="{{route('search')}}" method="POST">
                                @csrf
                                <div class="input-group">
                                    <input type="text" class="form-control search" name="txtSearch" id="autocomplete" required/>
                                    <button type="submit" class=" btn-success"><i class="fa fa-search"></i></button>
                                </div>
                            </form>
                        </div>
                       </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="idterm1" class="img-top col-md-2 col-xs-12 col-sm-12" style="margin-right:4%;">
                                   <a href="api/get-section/11" class="imgg" > <img src="img/law-cut-icon_36.png" alt=""></a>
                                    <a href="api/get-section/11"   class="active_not">베트남 법전보기</a>
                                </div>
                                <div id="idterm2" class="img-top col-md-2 col-xs-12 col-sm-12" style="margin-right:4%;">
                                    <a href="api/get-section/12"  class="imgg"><img src="img/law-cut-icon_33.png" alt="" ></a>
                                    <a  href="api/get-section/12"  class="active_not">베트남 시사법률</a>
                                </div>
                                <div id="idterm3" class="img-top col-md-2 col-xs-12 col-sm-12" style="margin-right:4%;">
                                  <a href="api/get-section/13"  class="imgg"><img src="img/law-cut-icon_30.png" ></a>
                                    <a href="api/get-section/13"  class="active_not" href="#">법률 질의응답</a>
                                </div>
                                <div id="idterm4" class="img-top col-md-2 col-xs-12 col-sm-12" style="margin-right: 3%;">
                                   <a href="api/get-section/14"  class="imgg"><img src="img/law-cut-icon_27.png"></a>
                                    <a href="api/get-section/14"  class="active_not" href="#">베트남 산업</a>
                                </div>
                                <div id="idterm5" class="img-top col-md-2 col-xs-12 col-sm-12" style="margin-rigth:0%;">
                                    <a href="api/get-section/15"  class="imgg"><img  src="img/law-cut-icon_25.png" alt=""></a>
                                    <a href="api/get-section/15"  class="active_not" href="#">투자 질의응답</a>
                                </div>
                            </div>

                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12 col-xs-12 col-sm-12" style="padding-right: 3%;">
                                <div id="iterm">
                                </div>
                                <div id="iterm2">
                                </div>
                              
                            </div>
                        </div>
                </div>
                <div class="col-md-3 col-xs-3 col-sm-3">
                    <img data-src="holder.js/200x200" src="{{asset('img/law-cut-icon_11.png')}}" data-holder-rendered="true" class="banner1">
                    <img data-src="holder.js/200x200" src="{{asset('img/law-cut-icon_39.png')}}" data-holder-rendered="true" class="banner2">
                    <ul class="list-group" style="margin-top:1%;" id="list-search">
                        <li id="list" class="list-group-item disabled"><strong>실시간 이슈 검색어</strong></li>
                        
                    </ul>
                </div>

            </div>
        </div>

        <footer>
            <div class="footer-copyright text-center py-3">© 2018 Copyright:
                <a href="#"> Hanbisoft</a>
            </div>
        </footer>
        <script>
            this.callFirstSection(11);
            this.callSection();
            this.countSearch();
            var arr = [];
            $.ajax({
                url: "{{ route('api.search') }}",
                type: "get",
                dateType: "text",
                success: function(JsonResult) {
                for (var i  in JsonResult) {
                    arr.push(JsonResult[i])
                }
                console.log(arr)
                    $('#autocomplete').autocomplete({
                        lookup: arr,
                        onSelect: function(suggestion) {
                        }
                    });   
                }
            });
           

           function callFirstSection(ids) {
               var id = ids;
            $.ajax({
                url: "{{ route('api.section') }}",
                type: "get",
                data: {
                    'id':id
                },
                dateType: "text",
                success: function(result) {
                 var htmlResult = "";
                Object.keys(result).forEach(function(key) {
                    var res = result[key].description;
                    var img = result[key].images;
                    var des = result[key].content;
                    var descrip = des.substr(0,200);

                    if(img === null) {
                        img = "uploads/nothumbail.png";
                    } 
                    htmlResult += "<ul class='list-unstyled'><li class='media'><img class='mr-3 img-thumbnail' src='" + img +"' alt=''><div class='media-body'> <h5 class='mt-0 mb-1'><a href='/api/get-detail/"+result[key].section+"'>" +"["+ result[key].chapter_name + "]" +"&nbsp;" + result[key].title + "</a></h5>"+"<p>" + descrip + "</div></li></ul>";
                })
                $("#iterm").html(htmlResult);
                }
            });
           }

           function callSection() {
            $.ajax({
                url: "http://218.234.17.80:6001/",
                type: "get",
                dateType: "text",
                success: function(result) {
                  var JSONObject  = JSON.parse(result)
                  var dataResult  = JSONObject.data;
                
                  var htmlResult = "";
                  Object.keys(dataResult).forEach(function(key) {
                    var res = dataResult[key].description;
                    var img = dataResult[key].images;
                    var des = dataResult[key].content;
                    var descrip = des.substr(0,200);
                    if(img === null) {
                      img = "uploads/nothumbail.png";
                    } 
                    htmlResult += "<ul class='list-unstyled'><li class='media'><img class='mr-3 img-thumbnail' src='" + img +"'><div class='media-body'> <h5 class='mt-0 mb-1'><a href='/api/get-detail/"+ dataResult[key].section+"'>" +"[" + dataResult[key].chapter_name +"]"+"&nbsp;"   +dataResult[key].title +"</a></h5>" + descrip +"</div></li></ul>";
                  })
                  $("#iterm2").append(htmlResult);
                }
            });
        }
           function countSearch() {
            $.ajax({
                url: "{{ route('api.count_search') }}",
                type: "get",
                dateType: "text",
                success: function(result) {
                  var htmlResult = "";
                  Object.keys(result).forEach(function(key) {
                    htmlResult += "<li class='list-group-item d-flex justify-content-between align-items-center'>"+ result[key].title + "<span class='badge badge-primary badge-pill'>" +result[key].solansearch +"</span></li>";
                  })
                  $("#list-search").append(htmlResult);
                }
            });
        }
           
        </script>
    </div>
@endsection
