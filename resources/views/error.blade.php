
  @extends('master') 
  @section('body') 
    <div class="container-fuild">
        <header class="nav header bg">
            <nav class="navbar navbar-light content">
                <img src="{{ asset('img/law-cut-icon_03.png') }}">
                <a class="navbar-brand" href="/">Home</a>
                
            </nav>
        </header>
        <div class="container-fuild content">
            <div class="row" style="margin-top:1%">
                <div class="col-md-12">
                       <div class="row">
                        <div class="col-md-12">
                            <form action="{{route('search')}}" method="POST">
                                @csrf
                                <div class="input-group col-md-12 col-xs-12 col-sm-12">
                                    <input type="text" class="form-control search" name="txtSearch" id="autocomplete" required value="{{ $title }}"/>
                                    <button type="submit" class=" btn-success"><i class="fa fa-search"></i></button>
                                </div>
                            </form>
                        </div>
                       </div>
                        <br>
                        <div class="row">
                          <div class="col-md-6 col-md-offset-3" style="margin:0 auto;padding:0; width:100%;">
                              <p class="text-infor">"{{ $title }}" <span class="text-danger">에 대한 검색결과가 없습니다.</span></p>
                              <ol>
                                <li>단어의 철자가 정확한지 확인해 보세요</li>
                                <li>한글을 영어로 혹은 영어를 한글로 입력했는지 확인해 보세요.</li>
                                <li>검색어의 단어 수를 줄이거나, 보다 일반적인 검색어로 다시 검색해 보세요.</li>
                                <li>두 단어 이상의 검색어인 경우, 띄어쓰기를 확인해 보세요.</li>
                                <li>검색 옵션을 변경해서 다시 검색해 보세요.</li>
                              </ol>
                          </div>
                        </div>  
        </div>
        </div>
        <footer>
            <div class="footer-copyright text-center py-3">© 2018 Copyright:
                <a href="#"> Hanbisoft</a>
            </div>
        </footer>
    </div>
@endsection