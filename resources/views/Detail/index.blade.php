@extends('master') 
@section('body') 
    <div class="container-fuild">
        <header class="nav header bg">
            <nav class="navbar navbar-light content">
                <img src="{{ asset('img/law-cut-icon_03.png') }}">
                <a class="navbar-brand" href="/">Home</a>
            </nav>
        </header>
        <div class="container">
            <div class="row" style="margin-top:1%">
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <form action="{{route('search')}}" style="width:100%;" method="POST">
                        @csrf
                        <div class="input-group col-md-12 col-xs-12 col-sm-12">
                            <input type="text" class="form-control search" name="txtSearch" id="autocomplete" required/>
                            <button type="submit" class=" btn-success"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                    <article class="content_text">
                        @foreach($result as $value)
                            <h4 class="title">{{$value->section_title}}</h4>
                            <p class="chapter">{{$value->chapter_title}}</p> 
                            <p>{!! $value->section_content !!}</p>
                            @if(!empty($value->part_title))
                            <p class="part">{!! $value->part_title !!}</p>
                            @endif
                            @if(!empty($value->tag))
                            <span class="tag label label-info">{!! $value->tag !!}</span>
                            @endif

                        @endforeach
                        <div class="col-xs-6 col-xs-offset-3 col-sm-4 col-sm-offset-0" style="margin:0 auto; clear:both;">
                            <a  href="/"><img style="width:90px; margin-top:2%;clear:both" src="{{ asset('img/button_03.png') }}"></a>
                        </div>    
                    </article>
                </div>    
            </div>
        </div>

        <footer>
            <div class="footer-copyright text-center py-3">© 2018 Copyright:
                <a href="#"> Hanbisoft</a>
            </div>
        </footer>
        <script>
                 var arr = [];
            $.ajax({
                url: "{{ route('api.search') }}",
                type: "get",
                dateType: "text",
                success: function(JsonResult) {
                for (var i  in JsonResult) {
                    arr.push(JsonResult[i])
                }
                console.log(arr)
                    $('#autocomplete').autocomplete({
                        lookup: arr,
                        onSelect: function(suggestion) {
                            console.log(suggestion)
                        }
                    });   
                }
            });
        </script>
    </div>
@endsection