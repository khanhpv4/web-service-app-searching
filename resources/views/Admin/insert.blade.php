@extends('Admin.master')
@section('body')
        <script src="//cdn.ckeditor.com/4.10.0/standard/ckeditor.js"></script>
     <div class="row">
            <div class="col-md-12">
                    @if (\Session::has('success'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! \Session::get('success') !!}</li>
                        </ul>
                    </div>
                @endif
              </div>
     </div>
       <form action="{{ route('admin.post') }}" method="POST" enctype="multipart/form-data">
           @csrf
            <div class="form-group">
                <label for="">Subject</label>
                <input type="text" class="form-control" name="title" required>
            </div>
            <div class="form-group">
                <label for="">Thumbnail</label>
                <input type="file" class="form-control" name="img" id="img" required>
            </div>    
            <div class="form-group">
            <label for="">Category</label>
                <select class="form-control" id="" name="chapter">
                        @if(Auth::user()->role == 1)
                        <option value="11" selected>베트남 법전보기 </option>
                        <option value="12">베트남 시사법률</option>
                        <option value="13">베트남 산업</option>
                        @endif
                        <option  value="14">법률 질의응답</option>
                        <option value="15">투자 질의응답</option>
                </select>
            </div>
            <div class="form-group">
                <label for="">Body</label>
                <textarea onchange="myFunction()" required name="content" id="content">
                </textarea>
            </div>
                <input type="hidden" class="form-control" id="description" name="description"  maxlength="500"/>
            <div class="form-group">
                <label for="">Tag</label>&nbsp;
                <input type="text" id="tag" name="tag" class="form-control" value="tag" data-role="tagsinput" />
            </div>
            <div style="text-align:center">
                <button class="btn btn-success">Save</button>
                <button class="btn btn-danger ">Cancer</button>
            </div>
            <div id="demo"></div>
        </form> 
        <script>
            CKEDITOR.replace( 'content' );
        </script>
        <script>
        $(function () {
            $("#tag").val()
            $('#tag').tagsinput();
        });
        
     
        </script>
@endsection
