@extends('Admin.master')
@section('title')
    User
@endsection
@section('body')
    
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Role</th>
                </tr>
            </thead>
            <tbody>
                    @foreach($user as $value)
                <tr>
                   
                    <td>{{ $value->id }}</td>
                    <td>{{ $value->name }}</td>
                    <td>{{ $value->email }}</td>
                    <td>@if($value->role == 1) <strong class="text-danger">Admin</strong>
                        @else
                            <p class="text-info">User Login</p>
                        @endif
                    </td>
                   
                </tr>
                @endforeach
            </tbody>
        </table>
@endsection
