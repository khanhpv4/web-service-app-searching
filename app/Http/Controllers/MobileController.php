<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MobileController extends Controller
{

    public function apiMobile(Request $request)
    {
        $title = $request->input('titleName');
        if(empty($title)) {
          return response('data not found');
        }
        $q_chapter = "SELECT chapter,title FROM chapter as c where c.title like '%$title%'";
        $chapter = DB::select(DB::raw($q_chapter));
        $chapter_id = '';
        $chapter_title = '';

        foreach ($chapter as $value) {
          $chapter_id = $value->chapter;
          $chapter_title = $value->title;
        }
        $q_part = "SELECT chapter, title FROM part WHERE chapter = $chapter_id ";
        $part = DB::select(DB::raw($q_part));

        $q_ses = "SELECT section,chapter,part, title, content FROM section as s WHERE chapter = $chapter_id";
        $session = DB::select(DB::raw($q_ses));

        $q_ses_part = "SELECT part,chapter,title FROM part as p WHERE chapter IN($chapter_id)";
        $ses_part = DB::select(DB::raw($q_ses_part));

        // RETURN RESULT
        if($chapter) {
            return response([
              'check' => 2,
              'chapter' => $chapter_title,
              'session' => $session,
              'part' => $ses_part
            ]);
        }
        return response()->json("error");
    }

  //TODO FUNCTION API DETAIL 
  public function Detail(Request $request) {
    $id = $request->input('id');
    if(!$id) {
      return 'id not found';
    }
    $q = "Select Title, Content FROM section WHERE section = $id";
    $result = DB::select(DB::raw($q));
    if($result) {
      return response($result);
    }
    return response("data not found");
  }

  //TODO FUNCTION GET ALL
  public function getAllChapter() {
    $q = "SELECT c.chapter AS c_chapter,c.title AS c_title FROM chapter c LEFT JOIN part p ON c.chapter = p.chapter";
    $result = DB::select(DB::raw($q));
    if($result) {
      return response($result);
    }
    return response("data not found");
  }

  //TODO FUNCTION GET PART 
  public function find(Request $request) {
    $id = $request->input('id');
    if(!$id) {
      return 'id not found';
    }
    $q =" SELECT s.section,s.title AS s_title, s.content AS s_content, p.title AS part_title,p.part AS part_id, c.title AS chapter_title FROM section s LEFT JOIN part p ON s.part = p.part LEFT JOIN chapter c ON s.chapter = c.chapter WHERE c.chapter = $id";

    $result = DB::select(DB::raw($q));
    if($result) {
      return response($result);
    }
    return response("data not found");
  }
}

