<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Chapter;
use App\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;

class AdminController extends Controller
{
    public function index()
    {
        return view('Admin.insert');
    }

    public function user()
    {
        $user = User::all();
        return view('Admin.user',compact('user'));
    }

    public function insert()
    {
        return view('Admin.insert');
    }

    public function add(Request $request)
    {
       $data = $request->all();
       $des = $request->get('description');
       if(empty($des)) {
           $str = $request->get('content');
           $des = str_limit($str,500);
       }
       $file = $request->file('img');
       $messages = [
        'image' => 'Định dạng không cho phép',
        'max' => 'Kích thước file quá lớn',
    ];
        // Điều kiện cho phép upload
        $this->validate($request, [
            'file' => 'image|max:2028',
        ], $messages);

        if ($request->file('img')->isValid()){
            // Lấy tên file
            $file_name = $request->file('img')->getClientOriginalName();
            // Lưu file vào thư mục upload với tên là biến $filename
            $urlFile = $request->file('img')->move('uploads',$file_name);
        }
       if(!$data) {
           return false;
       }
       $section = Session::create([
           'title' => $data['title'],
           'content' => $data['content'],
           'chapter' => $data['chapter'],
           'tag' => $data['tag'],
           'description' => $des,
           'images' => $urlFile
       ]);
       if($section) {
        return redirect()->back()->with('success','추가 완료'); 
       }
       return false;
    }

    public function getSection(Request $request)
    {
        $id = $request->input('id');
        if(!$id) {
            return false;
        }
        $q = "SELECT s.description,s.section,s.title,s.chapter,s.content,s.tag,s.images, c.title as chapter_name FROM section s
        LEFT JOIN chapter c ON s.chapter = c.chapter
    where s.chapter  = 11  
    ORDER BY s.section DESC
    LIMIT 1";
        // echo $q;die;
        $result = DB::select(DB::raw($q)); 
        if($result) {
            return $result;
        }
        return false;
    }

    public function getForsection() 
    {
        $q = "SELECT * FROM `section`
        WHERE section.chapter IN (12, 13, 14, 15) LIMIT 4";
        // echo $q;die;
        $result = DB::select(DB::raw($q)); 

        if($result) {
            return $result;
        }
        return false;   
    }   
    
    public function countSearch() 
    {
        $q = "SELECT COUNT(title) AS solansearch,title FROM search GROUP BY title ORDER BY solansearch DESC LIMIT 5";
        $result = DB::select(DB::raw($q));
        if($result){
            return $result;
        }
        return false;
    }
}
