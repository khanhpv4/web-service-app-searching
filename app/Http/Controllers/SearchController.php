<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Session;
use App\Chapter;
use App\Search;

class SearchController extends Controller
{
    //GET SECTION BY ID
    public function search(Request $request)
    {
        $title = $request->txtSearch;
        if(empty($title)) {
          return response('data not found');
        }
        $q = "SELECT * FROM section s
        WHERE s.title LIKE '%$title%'
        or s.tag LIKE '%$title%'";
        $chapter = DB::select(DB::raw($q));
        // return $chapter;die;
        if($chapter) {
            $search = Search::create([
                'title'=> $title
            ]);
           return view('Section.index',compact('chapter'));
        }
        return view('error',compact('title'));
    }
    
    //TODO GET ALL Section API
    public function getAll(Request $request)
    {
        $q = "SELECT title as value FROM section";
        $result = DB::select(DB::raw($q));

        if($result) {
            return response($result);
        }
        return false;
    }

    public function getAllSection(Request $request)
    {
        $q = "SELECT section,title, content FROM section limit 10";
        $result = DB::select(DB::raw($q));

        if($result) {
            return view('Home.index',compact('result'));
        }
        return false;
    }

    //TODO GET DETAIL SECTION
    public function detail($key)
    {
       $id =$key;
       if(!$id) {
           return false;
       }
       $q = "SELECT s.section as section_id,s.title as section_title,s.tag, s.content as section_content, p.part, p.title as part_title, c.chapter, c.title as chapter_title
       FROM section s
       LEFT JOIN part p ON s.part = p.part
       LEFT JOIN chapter c On s.chapter = c.chapter
       WHERE s.section = $id
       ORDER BY s.section";
       $result = DB::select(DB::raw($q));

    //    return $result;die;
       if($result){
           return view('Detail.index',compact('result'));
       }
       return false;
    }

    //TODO CHAPTER ALL
    function Chapter()
    {
       $result = Chapter::all()->random(5);
       if($result){
           return response($result);
       }
       return false;
    }

    //TODO KHI SELECT CHAPTER
    public function getChapter($key)
    {
        $id = $key;
        if(!$id){
            return false;
        }
        $q = "SELECT s.section as section_id,SUBSTRING(s.content,1,200) as section_des, s.images as section_image, s.tag as section_tag, s.section,s.title AS section_title,s.content AS section_content,p.title AS part_title,c.chapter AS chapter_id,c.title AS chapter_title
                FROM section s
                LEFT JOIN part p ON s.part = p.part
                LEFT JOIN chapter c ON c.chapter = s.chapter
                WHERE s.chapter  = $id  ORDER BY section DESC limit 2";
                
        // echo $q;die;    
        $result = DB::table('section AS s')
                    ->leftJoin('part', 's.part', '=', 'part.part')
                    ->leftJoin('chapter', 's.chapter', '=', 'chapter.chapter')
                    ->select('s.section as section_id','s.title as section_title','s.content as section_content')
                    ->where('s.chapter', '=', $id)
                     ->orderBy('s.section', 'desc')
                     ->paginate(10);

        if($result) {
            return view('Section.section-chapter',compact('result'));
        }     
        return view('Section.section-chapter');
    }
}
