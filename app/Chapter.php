<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    protected $table = 'chapter';
    protected $fillable = [
        'title', 'content','chapter',
    ];
    public $timestamps = false;
}
