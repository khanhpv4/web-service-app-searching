<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $table = 'section';
    protected $fillable = [
        'title', 'content','tag','chapter','images','description'
    ];
    public $timestamps = false;
}
