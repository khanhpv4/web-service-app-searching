<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','SearchController@getAllSection');

Route::post('/search','SearchController@search')->name('search');
Route::get('api/get-all','SearchController@getAll')->name('api.search');
Route::get('api/get-detail/{id}','SearchController@detail')->name('api.detail');
Route::get('api/api-chapter','SearchController@Chapter')->name('api.chapter');
Route::get('api/get-section/{id}','SearchController@getChapter')->name('api.getchapter');


//api mobile
Route::get('/api/search-mobile','MobileController@apiMobile');
Route::get('/api/search-detail','MobileController@Detail');
Route::get('/api/all-chapter','MobileController@getAllChapter');
Route::get('/api/all-part-section','MobileController@find');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Admin
Route::group(['middleware' => 'auth'], function () {
    Route::get('/admin','AdminController@index')->name('admin');
    Route::get('/admin/user','AdminController@user')->name('admin.user');
    Route::get('/admin/insert','AdminController@insert')->name('admin.insert');
    Route::post('/admin/add','AdminController@add')->name('admin.post');
});
//Api
Route::get('api/chapter-find','AdminController@getSection')->name('api.section');
Route::get('api/section-all','AdminController@getForsection')->name('api.section_all');
Route::get('api/get-search','AdminController@countSearch')->name('api.count_search');
